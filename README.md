# ts-react-native-barcode-scanner

Barcode Scanner for react native applications

## Installation

```sh
npm install ts-react-native-barcode-scanner
```

## Setup

* Install peer dependencies

```sh
yarn add react-native-reanimated react-native-vision-camera vision-camera-code-scanner
```

* Configure babel.config.js

```js
...
plugins: [
    ...
    [
      'react-native-reanimated/plugin',
      {
        globals: ['__scanCodes'],
      },
    ],
    ...
  ],
...
```

## Usage

* List devices from react-native-vision-camera using ```useCameraDevices``` hook 
  * ```const devices = useCameraDevices()```
* Select the device and pass to the BarcodeScanner component.
  * ```const device = devices.front```
* Use the callback function to get the list of barcodes detected.

### Sample full screen scanner:

```jsx
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useCameraDevices} from 'react-native-vision-camera';
import BarcodeScanner from 'ts-react-native-barcode-scanner';

const ScannerScreen = () => {
  const devices = useCameraDevices();
  const device = devices.back;
  return (
    <View style={{flex: 1}}>
      <Text>ScannerScreen</Text>
      {device && (
        <BarcodeScanner
          style={StyleSheet.absoluteFill}
          camera={device}
          callback={(barcodes) => {
            console.log(barcodes);
          }}
        />
      )}
    </View>
  );
};

export default ScannerScreen;

```

## License

MIT

---

Made with [create-react-native-library](https://github.com/callstack/react-native-builder-bob)
